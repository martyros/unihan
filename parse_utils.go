package unihan

import (
	"bufio"
	"bytes"
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

var reUnicodeText = regexp.MustCompile(`U\+([23]?[0-9A-F]{4})`)

func ParseUnicode(in string) (rune, error) {
	if sub := reUnicodeText.FindStringSubmatch(in); sub == nil {
		return 0, fmt.Errorf("Could not parse '%s' as unicode text", in)
	} else if keyint, err := strconv.ParseInt(sub[1], 16, 64); err != nil {
		return 0, fmt.Errorf("Converting hex '%s' to integer: %w", sub[1], err)
	} else {
		return rune(keyint), nil
	}
}

func ParseUnihan(input []byte, f func(string, rune, []string) error) error {
	scanner := bufio.NewScanner(bytes.NewReader(input))

	linere := regexp.MustCompile(`^U\+([23]?[0-9A-F]{4})\t(k\w+)\t(.*)$`)

	for scanner.Scan() {
		line := scanner.Text()

		// Skip comment lines and empty lines
		if len(line) == 0 || line[0] == '#' {
			continue
		}

		var propertyname string
		var key rune
		var propertyvalues []string

		if sub := linere.FindStringSubmatch(line); sub == nil {
			return fmt.Errorf("Unmatched line: %s", line)
		} else {
			if keyx, err := strconv.ParseInt(sub[1], 16, 64); err != nil {
				return fmt.Errorf("Parsing unicode hex %s: %v", sub[1], err)
			} else {
				key = rune(keyx)
			}

			propertyname = sub[2]

			// FIXME: Not all properties can be split (and some may have different split characters)
			propertyvalues = strings.Split(sub[3], " ")
		}

		if err := f(propertyname, key, propertyvalues); err != nil {
			return err
		}
	}

	return nil
}
