package unihan

// TraditionalVariant contains the [kTraditionalVariant] entries of the Unihan
// database.
//
// Description: "The Unicode value(s) for the traditional Chinese variant(s) for
// this character. A full discussion of the kSimplifiedVariant and
// kTraditionalVariant fields is found in [Section 3.7.1] of Unicode Standard
// Annex #38."
//
// [Section 3.7.1]: https://www.unicode.org/reports/tr38/tr38-33.html#SCTC
// [kTraditionalVariant]: https://www.unicode.org/reports/tr38/tr38-33.html#kTraditionalVariant
var TraditionalVariant map[rune][]rune

// TraditionalAlternate will look for an alternate Traditional character in the
// UniHan database for the given rune, which should be a simplified Chinese
// character.  It will return the first character it finds which is not
// identical to the character passed in.  If no alternate is found, a negative
// value will be returned.
func TraditionalAlternate(srune rune) rune {
	return FindAlternate(srune, TraditionalVariant)
}
