package unihan

import "testing"

func TestSimpTradVariants(t *testing.T) {
	for _, srune := range []rune{'这', '个', '会'} {
		trune := TraditionalAlternate(srune)

		if trune < 0 {
			t.Errorf("Couldn't find traditional alternate for %c", srune)
			continue
		}

		t.Logf("Traditional alternate for %c: %c", srune, trune)

		strune := SimplifiedAlternate(trune)

		if strune < 0 {
			t.Errorf("Couldn't find simplified alternate for %c", trune)
			continue
		} else if strune != srune {
			t.Errorf("Expected alternate %c, got %c", srune, strune)
			continue
		}

		t.Logf("Simplified alternate for %c: %c", trune, strune)

		if ttrune := TraditionalAlternate(trune); ttrune >= 0 {
			t.Errorf("ERROR: Unexpected traditional alternate %c for traditional character %c", ttrune, trune)
		}

		if ssrune := SimplifiedAlternate(srune); ssrune >= 0 {
			t.Errorf("ERROR: Unexpected simplified alternate %c for simplified character %c", ssrune, srune)
		}

	}

	if err := ParseVariants(); err != nil {
		t.Errorf("Error parsing variants: %v", err)
		return
	}
}

func TestFrequency(t *testing.T) {
	tests := []struct {
		r    rune
		want int
	}{
		{'出', 1},
		{'齣', 5},
	}

	for _, test := range tests {
		gotfreq, prs := Frequency[test.r]
		if !prs {
			t.Errorf("ERROR No frequency information for character %c", test.r)
			continue
		}
		if gotfreq != test.want {
			t.Errorf("ERROR: Frequency for character %c: Wanted %d, got %d!", test.r, test.want, gotfreq)
			continue
		}
	}

	if err := ParseDictionaryLikeData(); err != nil {
		t.Errorf("Error parsing dictionary-like-data: %v", err)
		return
	}

}
