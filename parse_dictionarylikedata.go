package unihan

import (
	_ "embed"
	"fmt"
	"strconv"
)

// Frequency contains the [kFrequency] entries of the Unihan database.
//
// Description: "A rough frequency measurement for the character based on
// analysis of traditional Chinese USENET postings; characters with a kFrequency
// of 1 are the most common, those with a kFrequency of 2 are less common, and
// so on, through a kFrequency of 5."
//
// [kFrequency]: https://www.unicode.org/reports/tr38/tr38-33.html#kFrequency
var Frequency map[rune]int

//go:embed Unihan_DictionaryLikeData.txt
var unihanDictionaryLikeDataSource []byte

func ParseDictionaryLikeData() error {
	Frequency = make(map[rune]int)

	return ParseUnihan(unihanDictionaryLikeDataSource, func(propertyname string, key rune, propertyvalues []string) error {
		switch propertyname {
		case "kFrequency":
			// Do nothing
		default:
			return nil
		}

		if len(propertyvalues) != 1 {
			return fmt.Errorf("Unexpected number of property values: %d", len(propertyvalues))
		}

		if freq, err := strconv.Atoi(propertyvalues[0]); err != nil {
			return fmt.Errorf("Parsing kFrequency value %s: %w", propertyvalues[0], err)
		} else {
			Frequency[key] = freq
		}

		return nil
	})
}

func init() {
	ParseDictionaryLikeData()
}
