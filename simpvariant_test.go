package unihan

import "testing"

func TestIsTraditional(t *testing.T) {
	tests := []struct {
		s    string
		want bool
	}{
		{"你好", false},
		{"", false},
		{"testing", false},
		{"这是我的朋友", false},
		{"這是我的朋友", true},
		{"你会不会帮忙吗", false},
		{"你會不會幫忙嗎", true},
	}

	for _, test := range tests {
		got := IsTraditional(test.s)
		t.Logf("'%s': %v", test.s, got)
		if test.want != got {
			t.Errorf("ERROR: Input %s wanted %v got %v!", test.s, test.want, got)
		}
	}

}

func TestIndicating(t *testing.T) {
	tests := []struct {
		r    rune
		want int
	}{
		{'出', IndNone},
		{'家', IndNone},
		{'了', IndNone},
		{'回', IndNone},
		{'面', IndNone},
		{'这', IndSimp},
		{'這', IndTrad},
		{'齣', IndTrad},
		{'迴', IndTrad},
		{'傢', IndTrad},
		{'只', IndNone},
		// {'戠', IndTrad},
		// {'隻', IndTrad},
		{'干', IndSimp},
		{'乾', IndTrad},
		// {'幹', IndTrad},
	}

	for _, test := range tests {
		got := Indicating(test.r)
		if got != test.want {
			t.Errorf("ERROR: Indicating(%c) wanted %d got %d!", test.r, test.want, got)
		}
	}

	// Make sure that our heuristic never indicates both
	for r := range TraditionalVariant {
		if IsIndicating(r, TraditionalVariant) && IsIndicating(r, SimplifiedVariant) {
			t.Errorf("ERROR: Character %c indicates both Traditional and Simplified!", r)
		}
	}

	for r := range SimplifiedVariant {
		if IsIndicating(r, TraditionalVariant) && IsIndicating(r, SimplifiedVariant) {
			t.Errorf("ERROR: Character %c indicates both Traditional and Simplified!", r)
		}
	}
}
