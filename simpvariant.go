package unihan

// SimplifiedVariant contains the [kSimplifiedVariant] entries of the Unihan
// database.
//
// Description: "The Unicode value(s) for the simplified Chinese variant(s) for
// this character. A full discussion of the kSimplifiedVariant and
// kTraditionalVariant fields is found in [Section 3.7.1] of Unicode Standard
// Annex #38."
//
// [Section 3.7.1]: https://www.unicode.org/reports/tr38/tr38-33.html#SCTC
// [kSimplifiedVariant]: https://www.unicode.org/reports/tr38/tr38-33.html#kSimplifiedVariant
var SimplifiedVariant map[rune][]rune

// SimplifiedAlternate will look for an alternate simplified character in the
// UniHan database for the given rune, which should be a traditional Chinese
// character.  It will return the first character it finds which is not
// identical to the character passed in.  If no alternate is found, a negative
// value will be returned.
func SimplifiedAlternate(trune rune) rune {
	return FindAlternate(trune, SimplifiedVariant)
}

// FindAlternate returns an alternate character for the rune r, given the
// variant map vmap.  Specifically, it returns the first rune in the variant map
// which is not equal to the rune itself.  If no alternates are found, a
// negative rune value is returned.
//
// [SimplifiedAlternate] and [TraditionalAlternate] are wrappers for this function,
// passing [SimplifiedVariant] and [TraditionalVariant], respectively.
//
// This is necessary because many characters have kTraditionalVariant properties
// which point to themselves.
func FindAlternate(orune rune, vmap map[rune][]rune) rune {
	arunes := vmap[orune]

	arune := rune(-1)
	for _, r := range arunes {
		if r != orune {
			arune = r
			break
		}
	}

	return arune
}

// IsIndicating determines whether the rune r represents an "indicating"
// character, which can be used to help classify a text as "simplified" or
// "traditional".  To indicate whether a character is "indicating traditional",
// pass [SimplifiedVariant]; to indicate whether a character is "indicating
// simplified", pass [TraditionalVariant].
//
// One simple heuristic to determine whether a character is "indicating" is to
// check to see whether the character has an alternate; for instance, '这' has a
// traditional alternate, but no simplified alternate; so we could say that '这'
// indicates a simplified text.
//
// This works when the simplification process involved merging a common,
// complicated character into a rare, simple character; this seems to have been
// the case with the traditional character 這, which was replaced with 这.  In
// this case, the heuristic "has a traditional alternate and no simplified
// alternate" correctly identifies '这' as simplified.
//
// The problem is that sometimes the simplification process involved swapping a
// rare, complicated character for a common, simple character; this seems to
// have been the case with the character '齣', which was merged into '出'.  The
// heuristic mentioned above incorrectly identifies '出' as indicating
// simplified; and looking only at the variants, there is no way to distinguish
// '这' from '出'.
//
// Other examples of the second type include 家,了,回, and 面.
//
// A character like 只 seems to be replacing two very similar characters with
// the simpler one.
//
// The current solution to this is to use the [Frequency] data; and say that a
// character can only be "indicating" if one of its alternates has the same or
// lower frequency than itself (keeping in mind here that lower frequency
// characters have a higher [Frequency] value).  This will cause '出' to *not* be
// classified as indicating anything, while classifying '齣' as indicating
// "traditional".
func IsIndicating(r rune, vmap map[rune][]rune) bool {
	rfreq, prs := Frequency[r]
	if !prs {
		return false
	}

	arunes := vmap[r]
	for _, arune := range arunes {
		if arune == r {
			continue
		}
		afreq, prs := Frequency[arune]
		if prs && afreq <= rfreq {
			return true
		}
	}
	return false
}

// Return values for [Indicating]
const (
	IndNone = 0    // Character isn't indicative of either simplified or traditional text
	IndSimp = iota // Character may indicate a simplified text
	IndTrad = iota // Character may indicate a traditional text
)

// Indicating returns whether rune r may indicate a simplified ([IndSimp]) or
// traditional ([IndTrad]) text, or neither [IndNone], using the algorithm
// described in [IsIndicating].
func Indicating(r rune) int {
	// We assume here that IsIndicating() will not return true for both
	// simplified and traditional; this is checked in simpvariant_test.go.

	// Check first whether it indicates traditional first, as there are probably
	// more "traditional-indicating" characters, and (given the above
	// assumption) means we can bail earlier.
	if IsIndicating(r, SimplifiedVariant) {
		return IndTrad
	}
	if IsIndicating(r, TraditionalVariant) {
		return IndSimp
	}
	return IndNone
}

// SimpTradCountWith calls [Indicating] on a rune-by-rune basis,
// collecting the runes which indicate a simplified or traditional text.
func SimpTradCountWith(s string) ([]rune, []rune) {
	var simp, trad []rune
	for _, r := range s {
		switch Indicating(r) {
		case IndTrad:
			trad = append(trad, r)
		case IndSimp:
			simp = append(simp, r)
		}
	}
	return simp, trad
}

// SimpTradCount calls [Indicating] on a rune-by-rune basis, returning a count
// of the runes which indicate simplified or traditional text.
func SimpTradCount(s string) (int, int) {
	var simp, trad int
	for _, r := range s {
		switch Indicating(r) {
		case IndTrad:
			trad++
		case IndSimp:
			simp++
		}
	}
	return simp, trad
}

// IsTraditional will attempt to asses if the string in question is a
// traditional Chinese text.  It does this by counting runes which indicate
// either a simplified or a traditional character set (as defined by
// [Indicating]), and returning 'true' if there are more traditional-indicating
// characters than simplified-indicating characters.
//
// This algorithm has been tested on a corpus of 3000 texts (albeit from a
// single source), each of which had both simplified and traditional variants,
// with no mistakes.
func IsTraditional(s string) bool {
	simp, trad := SimpTradCount(s)
	return trad > simp
}
