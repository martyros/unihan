package unihan

import (
	_ "embed"
)

//go:embed Unihan_Variants.txt
var unihanVariantsSource []byte

func ParseVariants() error {
	SimplifiedVariant = make(map[rune][]rune)
	TraditionalVariant = make(map[rune][]rune)

	return ParseUnihan(unihanVariantsSource, func(propertyname string, key rune, propertyvalues []string) error {
		switch propertyname {
		case "kTraditionalVariant", "kSimplifiedVariant":
			// Do nothing
		default:
			return nil
		}

		var runes []rune

		for _, ucodetext := range propertyvalues {
			if r, err := ParseUnicode(ucodetext); err != nil {
				return err
			} else {
				runes = append(runes, r)
			}
		}

		switch propertyname {
		case "kTraditionalVariant":
			TraditionalVariant[key] = append(TraditionalVariant[key], runes...)
		case "kSimplifiedVariant":
			SimplifiedVariant[key] = append(SimplifiedVariant[key], runes...)
		}

		return nil
	})
}

func init() {
	ParseVariants()
}
