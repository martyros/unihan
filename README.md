# Unihan

[![Go Reference](https://pkg.go.dev/badge/gitlab.com/laleo-language/unihan.svg)](https://pkg.go.dev/gitlab.com/laleo-language/unihan)

The immediate goal of this project is to be able to distinguish between
simplified and tradtional Chinese texts.  We do this by using the
`kSimplifiedVariant`, `kTraditionalVariant`, and `kFrequency` properties from
the Unicode UniHan database.  As such, the primary function is `IsTraditional`.
The core algorithm is described in the documentation for the `IsIndicating`
function.

The UniHan database, however, is a rich source of all kinds of additional
information.  The longer-team goal for this package is to expose that
information in a practical way.  Pull requests for parsing and exposing more
data are welcome.

# Adding additional properties

Please place parsing data in a file called `parse_$CATEGORY.go`, where
`$CATEGORY` is the category of the property.

For each property, create a separate file based on the property name.  In that
file, create a top-level variable `map[rune]T`, where T contains the appropriate
information for each rune.  Give it a comment similar to the comments on
`SimplifiedVariant`, `TraditionalVariant`, and `Frequency`: linking to the
original definition, and including the Description of the property for
convenience.

Additional functions using that data should live in the same file.

The long-term vision is to break each property down into a separate package, so
that users can include just the data that they need, rather than all parsed data
from the database.  Maintaining this code structure will make that refactoring
easier when it comes.
